"""_____________________________________________________________________

:PROJECT: LARA

*lara_metainfo urls *

:details: lara_metainfo urls module.
         - add app specific urls here

:file:    views.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          
:date: (last modification)  20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.urls import path, include
from django.views.generic import TemplateView

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from . import views

# Add your lara_metainfo urls here.

app_name = 'lara_metainfo' # !! this sets the apps namespace to be used in the template

# the 'name' attribute is used in templates to address the url independant of the view
# it is used/called by the {% url %} template tag
urlpatterns = [
    path('', views.index, name='index'),   
    path('metainfo-list/', views.metaInfoList, name='mi_list'),
    #~ path(r'^(?P<metainfo_id>[0-9]+)/$', views.metaInfoDetails, name='metainfo_details'),
    path('search/', views.searchForm, name='searchForm'),
    path('search-results/', views.search, name='search'),
    path('results/', views.results, name='results'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


