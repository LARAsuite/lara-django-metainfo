"""_____________________________________________________________________

:PROJECT: LARA

*lara_metainfo models *

:details: lara_metainfo database models. 
         Models for storing data meta information, like, e.g.,
         references, literature (class defines type JSON/XML/TXT)

:file:    models.py
:authors: mark doerr

:date: (creation)          20180625
:date: (last modification) 20190124

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.4"

import logging

from django.db import models

class ItemClass(models.Model):
    """ Meta information item classes or types like, e.g.,
        reference, literature, method (class defines type JSON/XML/TXT)
    """
    item_class_id = models.AutoField(primary_key=True)
    item_class = models.TextField(unique=True) # name of the class, like "reference", 
    description = models.TextField(blank=True)
                
    def __str__(self):
        return self.description or ''
        
    class Meta:
        db_table = "lara_metainfo_item_class"

class Categories(models.Model):
    """ generic categories  of substances, devices, containers, experiments,
    """
    category_id = models.AutoField(primary_key=True)
    name = models.TextField()
    slug = models.SlugField(max_length=255, unique=True)
    
    def __str__(self):
        return self.slug or ''

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

class Tags(models.Model):
    """ generic tags model 
    """
    tag_id = models.AutoField(primary_key=True)
    name = models.TextField() # tag name, like "reviewed", "interesing", "python", "evolution", ....
    
    def __str__(self):
        return self.name or ''

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

#redundand with FileField ?     
#~ class FileExternal(models.Model):
    #~ """ external file link """
    #~ externalfile_id = models.AutoField(primary_key=True)
    #~ filename =  models.TextField(blank=True)
    #~ path = models.TextField(blank=True)
    #~ full_filename = models.TextField(blank=True)
    #~ uuid = models.TextField(blank=True)
    #~ SHA256 = models.TextField(blank=True)
    #~ description = models.TextField(blank=True)
    
    #~ def __str__(self):
        #~ return self.full_filename or ''
    #~ class Meta:
        #~ db_table = "lara_metainfo_file_external"
     
class MetaInfo(models.Model):
    """ meta information and references """
    metainfo_id = models.AutoField(primary_key=True)
    metainfo_class = models.ForeignKey(ItemClass,on_delete=models.CASCADE,  blank=True, null=True) # reference, literature, spectrum (class defines type JSON/XML/TXT)
    metainfo = models.TextField(blank=True) # metainfo text, JSON, XML
    DOI =  models.TextField(blank=True)     # Digital Object Identifier
    bibTeX =  models.TextField(blank=True)  # bibTeX entry
    reference = models.TextField(blank=True) # bibliographic reference in a format specified by the metainfo_class
    bin_data = models.BinaryField(blank=True, null=True) # e.g. pdf, table, 
    # filepath ?
    #~ external_files = models.ManyToManyField('FileExternal', related_name='metainfo_external', blank=True) # for files that are too big to be stored in db
    description = models.TextField(blank=True)
            
    def __str__(self):
        return self.description or ''

