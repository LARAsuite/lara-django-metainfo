"""_____________________________________________________________________

:PROJECT: LARA

*lara_metainfo tests *

:details: lara_metainfo application tests.
         - 

:file:    tests.py
:authors: mark doerr

:date: (creation)          20180625
:date: (last modification) 20180625

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.test import TestCase
import logging

logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
#~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

# Create your tests here. s. https://docs.djangoproject.com/en/1.9/intro/tutorial05/

import datetime

from django.utils import timezone
from django.test import TestCase

from django.core.urlresolvers import reverse # needs to be changed in dango 1.10 to:
#~ from django.urls import reverse

from ..models import ItemClass, FileExternal, MetaInfo

def createMetainfo(num_items=1):
    """ Creates a list of metainfo entries
    """
    item_class_a = ItemClass.objects.create(item_class="test_class")

    meta_info_list = [ MetaInfo.objects.create(metainfo_class=item_class_a, metainfo="Test meta information %s" %i  ) for i in range(num_items) ]
    
    return meta_info_list

class MetainfoMethodsTests(TestCase):
    def testMetainfo(self):
        curr_mi = createMetainfo(num_items=10)
        
class MetaInfoViewsTests(TestCase):
    def testMetainfoView(self):
        """ Testing device view
        """
        curr_mi = createMetainfo()
        
        response = self.client.get(reverse('lara_metainfo:index'))
        
        #~ self.assertContains(response, "test")
        
    def testSearchFormView(self):
        """ Testing search  view
        """
        curr_mi = createMetainfo()
        
        response = self.client.get(reverse('lara_metainfo:searchForm'))
        
        self.assertContains(response, "Search")
