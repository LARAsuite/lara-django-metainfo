"""_____________________________________________________________________

:PROJECT: LARA

*lara_metainfo views *

:details: lara_metainfo views module.
         - 

:file:    views.py
:authors: mark doerr

:date: (creation)          20180625
:date: (last modification) 20180701

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

import sys

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse

#~ from django.core.urlresolvers import reverse  # for django 1.10 use : 
from django.urls import reverse

from .models import ItemClass, FileExternal, MetaInfo

def index(request):
    mi_list = MetaInfo.objects.all()
    context = {'mi_list': mi_list}
    return render(request, 'lara_metainfo/index.html', context)

def metaInfoList(request):
    metainfo_list = MetaInfo.objects.all()
    table_caption = "List of all LARA meta information"
    context = {'table_caption': table_caption, 'metainfo_list': metainfo_list}
    return render(request, 'lara_metainfo/metainfo_list.html', context)

def searchForm(request):
    return render(request, 'lara_metainfo/search.html', context={} )

def search(request):
    search_string = request.POST['search']
    
    try:
        mi_list = []
        mi = MetaInfo.objects.get(device__name = search_string)
        mi_list.append( mi )
        
    except ValueError as err:
        sys.stderr.write("ERROR:{}".format(err) )
        #~ return render(request, 'polls/detail.html', {
            #~ 'question': question,
            #~ 'error_message': "You didn't select a choice.",
        #~ })
    else:
        context = {'mi_list': mi_list}
        return render(request, 'lara_metainfo/results.html', context )
        #return HttpResponseRedirect(reverse('lara_metainfo:results', args=(lara_device_list,)))
        
def results(request, mi_list):
    #question = get_object_or_404(Question, pk=question_id)
    context = {'mi_list': mi_list}
    return render(request, 'lara_metainfo/results.html', context )

def metaInfoDetails(request, metainfo_id):
    mi = get_object_or_404(Device, pk=metainfo_id)
    context = {'metainfo': mi}
    return render(request, 'lara_metainfo/metainfo.html', context)
